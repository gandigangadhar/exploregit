import React, { Component } from 'react';
import {
    Alert
} from 'react-native';

import Aux from '../Aux/Aux';

const withErrorHanlder = (WrappedComponent, axios) => {

    return class extends Component {

        state = {
            error: null
        }

        constructor(props) {
            super(props);
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            });
            this.resInterceptor = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error});
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        errorConfirmedHandler = () => {
            this.setState({error: null});
        }

        showAlert = (errorMessage) => {
            return Alert.alert(
                'Error',
                errorMessage,
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
        }

        render() {
            return (
                <Aux>
                    {this.state.error ? this.showAlert(this.state.error.message) : null}
                    <WrappedComponent {...this.props} />
                </Aux>
            );
        }
    }
}

export default withErrorHanlder;