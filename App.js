import React, { Component } from "react";
import { 
  StyleSheet, 
  Text, 
  View,
  ActivityIndicator,
  StatusBar
} from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import ExploreGitNavigator from "./navigation/ExploreGitNavigator";
import reposReducer from "./store/reducers/repos";

const rootReducer = combineReducers({
  repo: reposReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

class App extends Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
      'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  /* fetchFonts = () => {
    return Font.loadAsync({
      'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
      'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
    });
  } */

  render() {
    const { fontLoaded } = this.state;
    console.log(this.state);

    if (fontLoaded) {
      return (
        <Provider store={store}>
          <ExploreGitNavigator />
        </Provider>
      );
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
      );
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default App;


/* 
 if (!this.state.fontLoaded) {
      return (
        <AppLoading
          startAsync={this.fetchFonts}
          onFinish={() => this.setState({ fontLoaded: true })}
        />
      );
    }

    return (
      <Provider store={store}>
        <ExploreGitNavigator />
      </Provider>
    );
*/