import * as actionTypes from './actionTypes';
import axios from 'axios';

export const onGetRepos = (searchString) => {
    return dispatch => {
        dispatch(getReposStart());
        //fetch('https://api.github.com/search/repositories?q='+searchString+'&sort=watchers&order=desc') //+'&sort=watchers&order=desc'
        //.then(response => response.json())
        axios.get('https://api.github.com/search/repositories?q='+searchString+'&sort=watchers&order=desc')
            .then(result => {
                console.log(result);
                dispatch(getReposSuccess(result.data.items));
            })
            .catch(error => {
                dispatch(getReposFail(error));
            })
    }
}

export const getReposStart = () => {
    return {
        type: actionTypes.GET_REPOS_START,
    }
}

export const getReposSuccess = (repos) => {
    return {
        type: actionTypes.GET_REPOS_SUCCESS,
        data: repos
    }
}

export const getReposFail = (error) => {
    return {
        type: actionTypes.GET_REPOS_FAIL,
        error: error
    }
}

export const getContibutorsList = (url) => {
    return dispatch => {
        dispatch(getContibutorsStart());
        fetch(url)
            .then(response => response.json())
            .then(data => {
                dispatch(getContibutorsSuccess(data))
            })
            .catch(error => {
                dispatch(getContibutorsFail(error))
            })
    }
}

export const getContibutorsStart = () => {
    return {
        type: actionTypes.GET_CONT_START
    }
}

export const getContibutorsSuccess = (contributors) => {
    return {
        type: actionTypes.GET_CONT_SUCCESS,
        data: contributors
    }
}

export const getContibutorsFail = error => {
    return {
        type: actionTypes.GET_CONT_FAIL,
        error: error
    }
}

export const getContibutorReposList = (url) => {
    return dispatch => {
        dispatch(getContibutorReposStart());
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log("repos actions",data);
                dispatch(getContibutorReposSuccess(data))
            })
            .catch(error => {
                dispatch(getContibutorReposFail(error))
            })
    }
}

export const getContibutorReposStart = () => {
    return {
        type: actionTypes.GET_CONT_REPOS_START
    }
}

export const getContibutorReposSuccess = (repos) => {
    return {
        type: actionTypes.GET_CONT_REPOS_SUCCESS,
        data: repos
    }
}

export const getContibutorReposFail = error => {
    return {
        type: actionTypes.GET_CONT_REPOS_FAIL,
        error: error
    }
}
