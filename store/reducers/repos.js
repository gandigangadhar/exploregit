import * as actionTypes from '../actions/actionTypes';

const initialState = {
    repos: [],
    loading: false,
    error: '',
    contributors: [],
    contributorRepos: []
}

const reposReducer = (state = initialState, action) => {

    switch(action.type) {
        case actionTypes.GET_REPOS_START: 
            return { ...state, loading: true, repos: [] };
        case actionTypes.GET_REPOS_SUCCESS:
            console.log(action.data.splice(0, 3));
            let tenRepos = action.data.splice(0, 10);
            let sortedRepos = tenRepos.sort(function(a, b){
                return b.watchers_count-a.watchers_count
            })
            return { ...state, repos: sortedRepos, loading: false, 
                contributors: [],  contributorRepos: []}
        case actionTypes.GET_REPOS_FAIL:
            return { ...state, error: action.error, loading: false }
        case actionTypes.GET_CONT_START: 
            return {
                ...state, loading: true,
                contributorRepos: []
            }
        case actionTypes.GET_CONT_SUCCESS:
            return {
                ...state, 
                contributors: action.data.splice(0, 10),
                loading: false
            }
        case actionTypes.GET_CONT_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case actionTypes.GET_CONT_REPOS_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.GET_CONT_REPOS_SUCCESS:
            return {
                ...state,
                loading: false,
                contributorRepos: action.data
            }
        case actionTypes.GET_CONT_REPOS_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: 
            return state;
    }
}

export default reposReducer;