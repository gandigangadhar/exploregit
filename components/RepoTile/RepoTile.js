import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native';

const RepoTile = (props) => {

    const { repo, onSelect } = props;

    return <TouchableOpacity 
                onPress={onSelect}>
        <View style={styles.repoItem}>
            <View style={styles.repoAvatarView}>
                <Image source={{uri: repo.item.owner.avatar_url}} 
                    style={styles.repoAvatarImage}/>
            </View>
            <View style={styles.repoInfo}>
                <Text style={styles.repoName}>{repo.item.name}</Text>
                <Text style={styles.repoFullName}>{repo.item.full_name}</Text>
                <Text style={styles.repoWatchersCount}>{repo.item.watchers_count}</Text>
            </View>
        </View>
    </TouchableOpacity>;
}

const styles = StyleSheet.create({
    repoItem: {
        height: Dimensions.get('window').height / 6,
        flexDirection: 'row',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        paddingTop: 13,
        paddingBottom: 13
    },
    repoAvatarView: {
        width: (Dimensions.get('window').height / 6) - 26, 
        height: (Dimensions.get('window').height / 6) - 26,
        justifyContent: 'center',
    },
    repoAvatarImage: {
        width: '100%', 
        height: '100%',
        borderRadius: 20
    },
    repoInfo: {
        marginLeft: 14,
        //paddingTop: 13 //(Dimensions.get('window').height / 6) / 10
    },
    repoName: {
        fontWeight: 'bold',
        paddingBottom: 3
    },
    repoFullName: {
        paddingBottom: 3
    },
    repoWatchersCount: {
        color: '#bbb'
    }
});

export default RepoTile;