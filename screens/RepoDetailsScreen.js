import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    FlatList,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import { getContibutorsList } from "../store/actions/repos";

class RepoDetailsScreen extends Component {

    componentDidMount() {
        console.log("cdm in repoDetailsScreen");
        let repo = this.props.navigation.getParam('repoDetails');
        //console.log(repo.item.contributors_url);
        this.props.onGetContributors(repo.item.contributors_url);
    }

    renderContributor(cont) {
        return <TouchableOpacity 
                onPress={() => this.props.navigation.navigate(
                    {routeName: 'ContributorDetails', params: { contDetails: cont } })}>
            <View style={styles.contAvatarView}>
                <Image source={{uri: cont.item.avatar_url}} 
                    style={styles.contAvatarImage}/>
            </View>
        </TouchableOpacity>;
    }

    render() {

        let repo = this.props.navigation.getParam('repoDetails');
        
        return (
            <ScrollView style={styles.screen}>
                <View style={styles.repoNameView}>
                    <Text style={styles.repoNameText}>{repo.item.name}</Text>
                </View>
                <View style={styles.repoOwnerAvatarView}>
                    <Image source={{uri: repo.item.owner.avatar_url}} 
                            style={styles.repoOwnerAvatarImage}/>
                </View>
                <View style={styles.repoInfo}>
                    <View style={styles.repoInfoView}>
                        <Text style={styles.repoInfoKey}>Name:</Text>
                        <Text style={styles.repoInfoValue}>{repo.item.name}</Text>
                    </View>
                    <View style={styles.repoInfoView}>
                        <Text style={styles.repoInfoKey}>Project Link:</Text>
                        <Text style={styles.repoInfoValue} multiline>{repo.item.html_url}</Text>
                    </View>
                    <View style={styles.repoInfoView}>
                        <Text style={styles.repoInfoKey}>Description:</Text>
                        <Text style={styles.repoInfoValue} multiline>{repo.item.description}</Text>
                    </View>
                </View>
                <View>
                    <FlatList 
                        keyExtractor={item => item.id.toString()}
                        data={this.props.contributors} 
                        renderItem={this.renderContributor.bind(this)} 
                        horizontal={true} />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        marginHorizontal: 14
    },
    repoNameView: {
        marginVertical: 12
    },
    repoNameText: {
        fontWeight: 'bold',
        fontSize: 24
    },
    repoOwnerAvatarView: {
        width: '100%',
        height: Dimensions.get('window').height / 4
    },
    repoOwnerAvatarImage: {
        width: '100%',
        height: '100%',
        borderRadius: 10
    },
    repoInfo: {
        marginTop: 14
    },
    repoInfoView: {
        paddingVertical: 5,
        flexDirection: 'row'
    },
    repoInfoKey: {
        fontWeight: 'bold',
        width: Dimensions.get('window').width/4
    },
    repoInfoValue: {
        fontSize: 12
    },
    contAvatarView: {
        width: Dimensions.get('window').width / 2,
        height: Dimensions.get('window').width / 2,
        marginRight: 12
    },
    contAvatarImage: {
        width: '100%',
        height: '100%',
        borderRadius: 10
    }
});

const mapStateToProps = state => {
    return {
        contributors: state.repo.contributors
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetContributors: (url) => dispatch(getContibutorsList(url))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RepoDetailsScreen);