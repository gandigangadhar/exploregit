import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { getContibutorReposList } from "../store/actions/repos";
import RepoTile from '../components/RepoTile/RepoTile';

class ContributorDetailsScreen extends Component {

    componentDidMount() {
        
        let contDetails = this.props.navigation.getParam('contDetails');
        console.log(contDetails.item.repos_url);
        this.props.onGetContributorRepos(contDetails.item.repos_url);
    }

    renderRepo = (repo) => {

        return <RepoTile repo={repo} 
            onSelect={() => this.props.navigation.navigate(
                {routeName: 'RepoDetails', params: { repoDetails: repo } })} />
    }

    render() {
        const { contRepos } = this.props;

        console.log("contRepos", contRepos);

        return (
            <View style={styles.screen}>
                <View style={styles.contNameView}>
                    <Text style={styles.contNameText}>{contRepos[0] && contRepos[0].name}</Text>
                </View>
                <View style={styles.contAvatarView}>
                    <Image source={{uri: contRepos[0] && contRepos[0].owner.avatar_url}} 
                            style={styles.contAvatarImage}/>
                </View>
                <View style={styles.contRepoListTitleView}>
                    <Text style={styles.contRepoListTitleText}>
                        Repo List
                    </Text>
                </View>
                <View style={styles.repoList}>
                    <FlatList 
                        keyExtractor={item => item.id.toString()}
                        data={contRepos} 
                        renderItem={this.renderRepo} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        marginHorizontal: 14
    },
    contNameView: {
        marginVertical: 12
    }, 
    contNameText: {
        fontWeight: 'bold',
        fontSize: 24
    }, 
    contAvatarView: {
        width: '100%',
        height: Dimensions.get('window').height / 4
    }, 
    contAvatarImage: {
        width: '100%',
        height: '100%',
        borderRadius: 10
    },
    contRepoListTitleView: { marginVertical: 12 }, 
    contRepoListTitleText: { 
        fontWeight: 'bold',
        fontSize: 24
    },
    repoList: {
        marginHorizontal: 12
    }
});

const mapStateToProps = state => {
    return {
        contRepos: state.repo.contributorRepos
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetContributorRepos: (url) => dispatch(getContibutorReposList(url))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContributorDetailsScreen);