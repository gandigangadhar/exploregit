import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
    TouchableHighlight
} from 'react-native';
import {connect} from 'react-redux';
import Search from 'react-native-search-box';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import axios from 'axios';

import { onGetRepos } from "../store/actions/repos";
import RepoTile from '../components/RepoTile/RepoTile';
import withErrorHanlder from '../hoc/withErrorHandler/withErrorHandler';
import { NetworkContext } from '../hoc/NetworkProvider/NetworkProvider';

class HomeScreen extends Component {

    static contextType = NetworkContext;

    state = {
        searchValue: ''
    }

    searchHandler = (text) => {
        if(!this.context.isConnected) {
            return;
        }
        //this.setState({searchValue: text});
        //this.props.onSearchRepos(this.state.searchValue);
        return new Promise((resolve, reject) => {
            this.setState({searchValue: text});
            this.props.onSearchRepos(text);
            resolve();
        });
    }

    onCancel = () => {
        return new Promise((resolve, reject) => {
            this.setState({searchValue: ''});
            //this.props.onSearchRepos(text);
            resolve();
        });
    }

    renderRepo = (repo) => {
        return <RepoTile repo={repo} 
            onSelect={() => this.props.navigation.navigate(
                {routeName: 'RepoDetails', params: { repoDetails: repo } })} />
    }

    render() {

        const { searchValue } = this.state;
        const { loading, repos } = this.props;

        let networkBar = null;
        if(!this.context.isConnected) {
        networkBar = <View style={styles.networkBar}>
            <Text>No Internet connection detected!</Text>
        </View>;
        }

        let repoList = null;
        if(loading) {
            repoList = <View style={styles.searchHintMsg}>
                <Text style={styles.searchHintMsgText}>Loading Repos...</Text>
            </View>;
        }
        else if(searchValue.length <= 0) {
            repoList = <View style={styles.searchHintMsg}>
                <Text style={styles.searchHintMsgText}>Please search any github repo</Text>
            </View>;
        } else {
            repoList = <View style={styles.repoList}>
                <FlatList 
                    keyExtractor={item => item.id.toString()}
                    data={repos} 
                    renderItem={this.renderRepo} 
                    keyboardShouldPersistTaps={'handled'} />
            </View>;
        }

        return (
            <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
                <View style={styles.screen}>
                    {networkBar}
                    <View style={styles.header}>
                        <View style={styles.headerFilterView}>
                            <TouchableHighlight onPress={() => this.props.navigation.navigate('FilterRepos')}>
                                <MaterialCommunityIcons name="filter-variant" size={24} />
                            </TouchableHighlight>
                        </View>
                        <View>
                            <Text style={styles.headerText}>GitRepo</Text>
                        </View>
                    </View>
                    <View style={styles.searchView}>
                        <Search
                            ref="search_box"
                            onChangeText={this.searchHandler}
                            returnKeyType="default"
                            onCancel={this.onCancel}
                            //onSearch={this.searchHandler}
                            />
                        {/* <TextInput style={styles.searchInput} value={searchValue}
                            onChangeText={this.searchHandler} /> */}
                    </View>
                    {repoList}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        fontWeight: 'bold'
    },
    networkBar: {
        height: Platform.OS ==='android' ? 60 : 90,
        paddingTop: Platform.OS ==='android' ? 30 : 50,
        paddingHorizontal: 15,
        backgroundColor: '#aaa',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        zIndex: 100
    },
    header: {
        height: 90,
        paddingTop: Platform.OS ==='android' ? 24 : 36,
        marginHorizontal: 15
    },
    headerFilterView: {
        alignItems: "flex-end"
    },
    headerText: {
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold',
        //paddingLeft: 14
        //fontFamily: 'open-sans-bold'
    },
    searchView: {
        marginHorizontal: 15,
        marginTop: Platform.OS ==='android' ? 5 : 15
    },
    searchInput: {
        height: 40,
        borderColor: 'grey',
        borderWidth: 1
    },
    repoList: {
        marginHorizontal: 12
    },
    searchHintMsg: {
        alignItems: 'center',
        marginTop: Dimensions.get('window').height / 5
    },
    searchHintMsgText: {
        color: '#6836A2',
        fontWeight: '600',
        fontSize: 18
    }
});

const mapStateToProps = state => {
    return {
        repos: state.repo.repos,
        loading: state.repo.loading,
        error: state.repo.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSearchRepos: (searchValue) => dispatch(onGetRepos(searchValue))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHanlder(HomeScreen, axios));

//props.navigation.navigate('SomeIdentifier');

//<Button title="Go To Repo details" 
//                  onPress={() => this.props.navigation.navigate('RepoDetails')} />


/* 


        /* return <TouchableOpacity 
                    onPress={() => this.props.navigation.navigate(
                        {routeName: 'RepoDetails', params: { repoDetails: repo } })}>
                <View style={styles.repoItem}>
                    <View style={styles.repoAvatarView}>
                        <Image source={{uri: repo.item.owner.avatar_url}} 
                            style={styles.repoAvatarImage}/>
                    </View>
                    <View style={styles.repoInfo}>
                        <Text style={styles.repoName}>{repo.item.name}</Text>
                        <Text style={styles.repoFullName}>{repo.item.full_name}</Text>
                        <Text style={styles.repoWatchersCount}>{repo.item.watchers_count}</Text>
                    </View>
                </View>
            </TouchableOpacity>; */
