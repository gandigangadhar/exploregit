import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Switch
} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class FilterReposScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stars: false
        }
    }

    render() {
        return <View style={styles.screen}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Apply Filter Repos</Text>
            </View>
            <View style={styles.filtersContainer}>
                <View style={styles.filterContainer}>
                    <Text style={styles.filterContainerText}>Stars:</Text>
                    <Switch
                        value={this.state.stars}
                        onValueChange={() => {
                            this.setState({stars: !this.state.stars})
                        }}
                    />
                </View>
            </View>
            <View style={styles.filterActions}>
                <TouchableWithoutFeedback onPress={() => {this.props.navigation.pop()}}>
                    <View style={styles.filterActionView}>
                        <Text>Cancel</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {this.props.navigation.pop()}}>
                    <View style={[styles.filterActionView, styles.primaryFilterView]}>
                        <Text style={styles.primaryFilterText}>Apply</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 90,
        paddingTop: 56, //36
        marginHorizontal: 15,
        alignItems: 'center'
    },
    headerText: {
        color: '#6836A2',
        fontSize: 24
    },
    filtersContainer: {
        paddingTop: 15
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: "space-around"
    },
    filterContainerText: {
        fontSize: 20
    },
    filterActions: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 35
    },
    filterActionView: {
        padding: 10,
        paddingHorizontal: 20,
        borderWidth: 1,
        borderColor: '#ddd',
        backgroundColor: '#ddd'
    },
    primaryFilterView: {
        backgroundColor: '#6836A2'
    },
    primaryFilterText: {
        color: 'white'
    }
});

FilterReposScreen.navigationOptions = {
    headerShown: false,
}

export default FilterReposScreen;