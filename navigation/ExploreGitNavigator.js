import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import RepoDetailsScreen from '../screens/RepoDetailsScreen';
import ContributorDetailsScreen from '../screens/ContributorDetailsScreen';
import FilterReposScreen from '../screens/FilterReposScreen';

const ExploreGitNavigator = createStackNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                headerShown: false,
            },
        },
        RepoDetails: {
            screen: RepoDetailsScreen
        },
        ContributorDetails: ContributorDetailsScreen,
        FilterRepos: FilterReposScreen
    }, 
    {
        defaultNavigationOptions: {
            headerTitle: ''
        }
    }
);

export default createAppContainer(ExploreGitNavigator);